﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/*
    https://tortoisesvn.net/docs/release/TortoiseSVN_en/tsvn-dug-settings.html
    Pre-commit
    PATH DEPTH MESSAGEFILE CWD

    PATH
    A path to a temporary file which contains all the paths for which the operation was started. Each path is on a separate line in the temp file.

    Note that for operations done remotely, e.g. in the repository browser, those paths are not local paths but the urls of the affected items.
*/

namespace TortoiseSVNPreCommitTool
{
    class Program
    {
        static void Main(string[] args)
        {
            var listener = new TextWriterTraceListener(GetLogFilePath());
            Trace.Listeners.Add(listener);

            try
            {
                if (args.Length <= 0)
                {
                    Trace.TraceError("need parameters.");
                    return;
                }

                var temporaryFilePath = args[0];
                if (!File.Exists(temporaryFilePath))
                {
                    Trace.TraceError("Temporary file:{0} not exist.", temporaryFilePath);
                    return;
                }

                Trace.TraceError("Temporary file:{0}.", temporaryFilePath);
                var commitPaths = File.ReadAllLines(temporaryFilePath, System.Text.Encoding.Default);
                CheckUTF8BOM.Process(commitPaths);
            }
            catch (Exception e)
            {
                Trace.TraceError("Exception:{0}.", e);
            }
            finally
            {
                listener.Flush();
            }
        }

        static string GetLogFilePath()
        {
            var exeFilePath = System.Diagnostics.Process.GetCurrentProcess().MainModule.FileName;
            var exeDirectory = Path.GetDirectoryName(exeFilePath);
            var fileName = string.Format("snv_{0}.log", System.DateTime.Now.ToString("yyyy_MM_dd_HH_mm_ss"));
            var filePath = Path.Combine(exeDirectory, fileName);
            return filePath;
        }
    }

    class CheckUTF8BOM
    {
        static System.Text.Encoding Utf8Encoding = new System.Text.UTF8Encoding(true);

        public static void Process(string[] commitPaths)
        {
            foreach (var filePath in commitPaths)
            {
                ChangeToUtf8(filePath);
            }
        }

        private static void ChangeToUtf8(string filePath)
        {
            if (!File.Exists(filePath))
            {
                // Trace.TraceInformation("File:{0} not exist.", filePath);
                return;
            }

            if (!IsSourceFile(filePath))
            {
                Trace.TraceInformation("File:{0} is not source file.", filePath);
                return;
            }

            var content = File.ReadAllText(filePath);
            if (!NeedChangeToUtf8(content))
            {
                Trace.TraceInformation("File:{0} does not need to be converted to uft8.", filePath);
                return;
            }

            File.WriteAllText(filePath, content, Utf8Encoding);
            Trace.TraceInformation("File:{0} converted to uft8.", filePath);
        }

        private static bool IsSourceFile(string filePath)
        {
            var ext = Path.GetExtension(filePath);
            ext = ext .ToLower();

            return ext == ".cpp" || ext == ".cc" || ext == ".c" || ext == ".h";
        }

        private static bool NeedChangeToUtf8(string content)
        {
            foreach (var ch in content)
            {
                if ((ushort)ch > 127)
                    return true;
            }
            return false;
        }
    }
}
