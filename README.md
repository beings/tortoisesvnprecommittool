# `TortoiseSVN` pre-commit tool

## 解决问题

我用`Xcode`开发，同事用`Visual Studio`开发，对于有中文字符的源文件，`Xcode`将其转为无BOM头的UTF-8文件，会导致`Visual Studio`编译出错，需将文件转化为带BOM头的UTF-8文件。

这个工具就是在SVN提交前，检查待提交的文件是否需要添加BOM头。


## 用法
如下图修改`TortoiseSVN`的Settings

![TortoiseSVN Settings](usage.png)


**注意：** `Working Copy Path:` 是SVN库所在的文件夹（可以是svn库的父文件夹或根文件夹） 

